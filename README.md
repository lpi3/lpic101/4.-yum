# Certification LPIC 1 - 101-500

Bonjour et bienvenue dans ce scénario créé par **Jordan ASSOULINE** pour vous aider à maîtriser les commandes permettant de visualiser et d'installer des paquets sur une distribution CentOS/RedHat.

<br/>

---

# Exercice 1

Dans ce premier exercice, nous allons utiliser la commande `yum` pour installer des paquets.

Mais tout d'abord, plaçons-nous dans une situation où nous avons entendu parler d'une commande appelée `cowsay` qui affiche dans le terminal du texte en format ASCII et que nous avons envie d'essayer.

## Essayer la commande cowsay

1. Exécutez la commande `cowsay` dans votre terminal et observez la réponse<br/>

   

Si vous utilisez la commande `cowsay` directement dans votre terminal, vous obtiendrez un message indiquant 
*Command not found*.

Cela signifie que le système n’a pas trouvé la commande à laquelle vous faîtes référence. Le paquet correspondant n’est donc sans doute pas installé.

<br/>

## Recherche avec yum search

Vous pouvez effectuer des recherches sur les dépôts de paquets officiels en utilisant la commande : `yum search nom_paquet` ou `dnf search nom_paquet`. 

<br/>

En effet, la commande **yum search** est utilisée pour rechercher des paquets et afficher les informations correspondantes aux paquets disponibles pour être installés.

<br/>

Ainsi la commande `yum search cowsay` effectue une recherche pour toutes les occurences du terme "cowsay" dans les noms des paquets disponibles et dans leurs descriptions.

<br/>

2. Exécutez la commande `yum search cowsay` dans votre terminal et observez la réponse 

<br/>

---

# Exercice 2

La recherche a permis d’identifier, entre autres, un paquet appelé cowsay qui correspond à la commande manquante que nous avons essayée précédemment. 

<br/>

## Point sur l'utilisation de Sudo

L’installation ou la désinstallation d’un paquet nécessitent les permissions particulières de l’administrateur du système appelé **root**. 

<br/>

Sur la plupart des systèmes, les utilisateurs lambdas, peuvent demander temporairement les droits administrateurs grâce à la commande `sudo` (le système demandera d’entrer à nouveau leur mot de passe) à condition que l’administrateur en ait laissé la possibilité.

<br/>

Dans notre situation, pas besoin d'utiliser cette commande, car nous sommes déjà authentifié en tant que **root**

<br/>

## Installation d'un paquet

Pour les paquets au format **RPM**, l'installation s'effectue par la commande `yum install nom_paquet` ou `dnf install nom_paquet`.

<br/>

3. Effectuez la commande permettant d'installer le paquet **cowsay**.

<details><summary> Montrer la solution </summary>
<p>
yum install cowsay
</p>
</details>

*Remarque: si vous voulez simplement télécharger le paquet avec yum, sans pour autant l'installer, vous pouvez utilisez l'option --downloadonly*

<br/>

Une fois le téléchargement et l'installation terminés, les fichiers vont être copiés à leurs propres localisation et la commande va devenir disponible.

<br/>

4. Essayez donc : `cowsay Vive Linux!`

<br/>

## Mise à jour d'un paquet

Pour mettre à jour un paquet, il est nécessaire d'utiliser l'option **update** (Attention à ne pas confondre avec apt, ou update permet de mettre à jour l'index)

<br/>

5. Essayez donc de mettre à jour le paquet **unzip** grâce à la commande `yum update unzip`

*Remarque: Si vous ne mettez pas de nom de paquet après votre commande "yum update", c'est l'ensemble du système qui sera mis à jour.*

<br/>

Pour vérifier si une mise à jour est disponible, vous pouvez utiliser l'option **check-update**.

Par exemple, pour vérifier si le paquet **cowsay** possède une mise à jour : `yum check-update cowsay`

<br/>

6. Vérifiez si le paquet **cowsay** possède une mise à jour

<br/>

## Obtenir des informations sur un paquet

Pour obtenir des informations complémentaires sur un paquet, vous pouvez utiliser l'option **info** de la commande **yum**.

7. Par exemple pour firefox, exécutez la commande suivante : `yum info firefox`

<br/>

---

# Exercice 3

Dans ce troisième exercice, nous allons utiliser la commande `yum` pour désinstaller des paquets.

<br/>

## Désinstaller le paquet cowsay

Pour le moment, la commande cowsay est toujours disponible. Vous pouvez vérifier avec la commande: `cowsay Hello!`

<br/>

Les mêmes commandes prennent en paramètre le mot **remove** plutôt qu'**install** pour procéder à la désinstallation des paquets.

<br/>

8. Grâce à la commande `yum remove`, désinstallez le paquet : `cowsay` sur cette machine.

*Remarque: la commande sudo est aussi nécessaire pour désinstaller les paquets lorsque nous ne sommes pas authentifiés avec l'utilisateur root*

<details><summary> Montrer la solution </summary>
<p>
yum remove cowsay
</p>
</details>

<br/>

Vous pouvez vérifiez que la désinstallation s'est correctement déroulée, en essayant à nouveau la commande `cowsay Hello!`

<br/>

## Trouver le paquet lié à un fichier

Dans un des exemples précédents, nous avions cherché à installer le paquet **gimp** (qui est un éditeur d’image), ce qui avait échoué avec rpm à cause de problèmes de dépendances. En effet, rpm a indiqué quels étaient les fichiers manquants, mais pas forcément le nom des paquets associés à eux.

<br/>

9. Retrouvons ces informations en exécutant les commandes suivantes :
   `wget http://mirror.centos.org/centos/7/os/x86_64/Packages/gimp-2.8.22-1.el7.x86_64.rpm && rpm -i gimp-2.8.22-1.el7.x86_64.rpm`{{execute}}

<br/>

Ici une des dépendances manquantes correspond au fichier **libgimpui-2.0.so.0**.

Grâce à la commande **yum** et à l'option **whatprovides** on peut ainsi voir quel paquet est à l'origine du fichier **libgimpui-2.0.so.0**.

<br/>

**Q1: Quel paquet est à l'origine du fichier libgimpui-2.0.so.0 ?**
<details><summary> Montrer la solution </summary>
<p>
yum whatprovides libgimpui-2.0.so.0
</p>
</details>

<br/>

**Q2: Quel paquet est à l'origine du fichier /etc/hosts ?**
<details><summary> Montrer la solution </summary>
<p>
yum whatprovides /etc/hosts
</p>
</details>

<br/>

